
Available profiles
-
1. 
    1. all-tests
    2. allTests
2. 
   1. integration-tests
   2. integrationTests
3. 
   1. database-tests
   2. databaseTests

Build
-
_maven clean package_  
_maven -Pall-tests clean package_

run with custom properties
-
java -jar target/betbull-1.0-SNAPSHOT.jar --spring.config.location=sample.properties

for docker
=
build image by executing
-
> ./build.sh

start by executing
-
> ./run.sh\

Possible arguments
-
> ./run.sh expose -> should expose container accessibility to outside network


I didn't write any requests outside of integration tests\
I've used swagger, but I've never written any