package recruitment.betbull.data.entity;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import recruitment.betbull.data.entity.composites.PlayerTeamId;

import javax.persistence.*;
import java.time.LocalDate;

@NoArgsConstructor
@AllArgsConstructor
@Getter
@Setter
@Entity(name = "player_team")
@IdClass(PlayerTeamId.class)
public class PlayerTeamEntity {
    @EmbeddedId
    PlayerTeamId playerTeamId;

    @ManyToOne(cascade = CascadeType.ALL)
    @MapsId("playerId")
    private PlayerEntity playerId;

    @ManyToOne(cascade = CascadeType.ALL)
    @MapsId("teamId")
    private TeamEntity teamId;

    @Column
    private LocalDate memberFrom;

    @Column
    private LocalDate memberTo;

}
