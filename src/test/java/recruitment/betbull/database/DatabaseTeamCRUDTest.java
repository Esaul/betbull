package recruitment.betbull.database;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Tag;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.util.CollectionUtils;
import recruitment.betbull.constants.TestTags;
import recruitment.betbull.data.base.Team;
import recruitment.betbull.data.entity.TeamEntity;
import recruitment.betbull.exceptions.TeamDoesNotExistException;
import recruitment.betbull.spring.services.TeamService;

import javax.persistence.EntityManager;
import javax.transaction.Transactional;
import java.util.Currency;

import static org.junit.jupiter.api.Assertions.*;

@SpringBootTest
@Tag(TestTags.DATABASE)
class DatabaseTeamCRUDTest {
    private TeamEntity validTeam;

    @Autowired
    private TeamService teamService;

    @Autowired
    @Qualifier("entityManager")
    private EntityManager em;

    @BeforeEach
    @Transactional(Transactional.TxType.REQUIRED)
    void cleanup() {
        final Page<TeamEntity> allTeams = teamService.findAll(Pageable.unpaged());
        allTeams.stream().map(TeamEntity::getId).forEach(teamService::delete);

        validTeam = new TeamEntity(null, new Team("teamName", Currency.getInstance("PLN")));
    }

    @Test
    @Transactional(Transactional.TxType.REQUIRED)
    void shouldCreateTeam() {
        final TeamEntity created = teamService.create(validTeam);

        final TeamEntity found = assertDoesNotThrow(() -> teamService.findById(created.getId()), "Service shouldn't throw on valid entity");

        assertEquals(validTeam.getName(), created.getName());
        assertEquals(validTeam.getCurrency(), created.getCurrency());
        assertTrue(CollectionUtils.isEmpty(created.getPlayerTeams()));
        assertEquals(validTeam.getName(), found.getName());
        assertEquals(validTeam.getCurrency(), found.getCurrency());
        assertTrue(CollectionUtils.isEmpty(found.getPlayerTeams()));
    }

    @Test
    @Transactional(Transactional.TxType.REQUIRED)
    void shouldUpdateTeam() {
        final String updatedName = "updated";
        final Currency updatedCurrency = Currency.getInstance("EUR");

        final TeamEntity created = teamService.create(validTeam);
        em.clear();

        created.setName(updatedName);
        created.setCurrency(updatedCurrency);

        final TeamEntity found = assertDoesNotThrow(() -> teamService.update(created), "Service shouldn't throw on valid entity");

        assertEquals(updatedName, found.getName());
        assertEquals(updatedCurrency, found.getCurrency());
    }

    @Test
    @Transactional(Transactional.TxType.REQUIRED)
    void shouldDeleteExistingTeam() {
        final TeamEntity createdTeam = teamService.create(validTeam);
        em.clear();
        final Long teamId = createdTeam.getId();
        teamService.delete(teamId);

        assertThrows(TeamDoesNotExistException.class, () -> teamService.findById(teamId), "Entity shouldn't be available after deletion");
    }
}