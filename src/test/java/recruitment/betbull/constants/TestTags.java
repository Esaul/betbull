package recruitment.betbull.constants;

public interface TestTags {
    String INTEGRATION = "integration";
    String UNIT = "unit";
    String DATABASE = "database";
}
