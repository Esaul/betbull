package recruitment.betbull.spring.services;

import org.junit.jupiter.api.Tag;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.mockito.junit.jupiter.MockitoExtension;
import recruitment.betbull.constants.TestTags;
import recruitment.betbull.data.base.Player;
import recruitment.betbull.data.base.Team;
import recruitment.betbull.data.dto.PlayerTransferResult;
import recruitment.betbull.data.entity.PlayerEntity;
import recruitment.betbull.data.entity.PlayerTeamEntity;
import recruitment.betbull.data.entity.TeamEntity;
import recruitment.betbull.data.entity.composites.PlayerTeamId;
import recruitment.betbull.exceptions.TeamNotFoundException;
import recruitment.betbull.spring.repositories.PlayerRepository;

import java.math.BigDecimal;
import java.math.RoundingMode;
import java.time.LocalDate;
import java.time.temporal.ChronoUnit;
import java.util.Currency;
import java.util.HashSet;
import java.util.Optional;
import java.util.Set;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.when;

@ExtendWith(MockitoExtension.class)
@Tag(TestTags.UNIT)
class PlayerServiceTest {
    private static final Long playerId = 1L;
    private static final Long playerWithTeamId = 2L;
    private static final Long teamId = 1L;
    private static final Long invalidTeamId = 10L;
    private static final LocalDate exampleDateFromPast = LocalDate.of(2020, 1, 1);

    @InjectMocks
    PlayerService playerService;

    @Mock
    TeamService teamService;

    @Mock
    PlayerRepository playerRepository;

    public PlayerServiceTest() {
        MockitoAnnotations.openMocks(this);
        final PlayerEntity existingPlayer = new PlayerEntity(playerId, new Player("playerName", "playerSurname", exampleDateFromPast));
        final TeamEntity existingTeam = new TeamEntity(teamId, new Team("teamName", Currency.getInstance("PLN")));

        final PlayerEntity existingPlayerWithTeams = new PlayerEntity(playerWithTeamId, new Player("playerName", "playerSurname", exampleDateFromPast));
        Set<PlayerTeamEntity> playerTeamAssociations = new HashSet<>();
        PlayerTeamEntity playerTeamAssociation =
                new PlayerTeamEntity(new PlayerTeamId(playerWithTeamId, teamId), existingPlayerWithTeams, new TeamEntity(), exampleDateFromPast, null);
        playerTeamAssociations.add(playerTeamAssociation);
        existingPlayerWithTeams.setPlayerTeams(playerTeamAssociations);

        when(playerRepository.findById(playerId)).thenReturn(Optional.of(existingPlayer));
        when(playerRepository.findById(playerWithTeamId)).thenReturn(Optional.of(existingPlayerWithTeams));
        when(playerRepository.save(any())).thenAnswer(invocation -> invocation.getArgument(0));
        when(teamService.findById(teamId)).thenReturn(existingTeam);
    }

    @Test
    void shouldThrowWhenPlayerDoesntHaveTeam() {
        assertThrows(TeamNotFoundException.class, () -> playerService.removeTeam(playerId, invalidTeamId));
    }

    @Test
    void shouldAddNewTeamOnTeamTransferWhenNoPreviousTeamsPresent() {
        final PlayerTransferResult playerTransferResult = playerService.transferTeam(playerId, teamId);

        assertEquals(BigDecimal.ZERO, playerTransferResult.getTransferFee());
        assertEquals(BigDecimal.ZERO, playerTransferResult.getTransferCommission());
        assertEquals(BigDecimal.ZERO, playerTransferResult.getTotal());
        assertEquals(playerId, playerTransferResult.getPlayer().getId());
        assertEquals(1, playerTransferResult.getPlayer().getPlayerTeams().size());
        final TeamEntity team = playerTransferResult.getPlayer().getPlayerTeams().iterator().next().getTeamId();
        assertEquals(teamId, team.getId());
    }

    @Test
    void shouldUpdateLastTeamWhenPresent() {
        final long testMonthsPassed = ChronoUnit.MONTHS.between(exampleDateFromPast, LocalDate.now());
        final long testYearsPassed = ChronoUnit.YEARS.between(exampleDateFromPast, LocalDate.now());
        final BigDecimal expectedTransferFee = BigDecimal.valueOf(testMonthsPassed * 100_000 / testYearsPassed);
        final BigDecimal expectedTransferCommission = expectedTransferFee.divide(BigDecimal.valueOf(10), RoundingMode.HALF_EVEN);
        final BigDecimal expectedTransferTotal = expectedTransferFee.add(expectedTransferCommission);

        final PlayerTransferResult playerTransferResult = playerService.transferTeam(playerWithTeamId, teamId);

        assertEquals(expectedTransferFee, playerTransferResult.getTransferFee());
        assertEquals(expectedTransferCommission, playerTransferResult.getTransferCommission());
        assertEquals(expectedTransferTotal, playerTransferResult.getTotal());
        assertEquals(playerWithTeamId, playerTransferResult.getPlayer().getId());
        assertEquals(2, playerTransferResult.getPlayer().getPlayerTeams().size());
    }
}