package recruitment.betbull.spring.endpoints;

import com.fasterxml.jackson.annotation.JsonAutoDetect;
import com.fasterxml.jackson.annotation.PropertyAccessor;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.datatype.jsr310.JavaTimeModule;
import lombok.SneakyThrows;
import org.hamcrest.Matchers;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Tag;
import org.junit.jupiter.api.Test;
import org.mockito.ArgumentCaptor;
import org.mockito.Captor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.MockMvc;
import recruitment.betbull.constants.TestTags;
import recruitment.betbull.data.base.Team;
import recruitment.betbull.data.dto.TeamForm;
import recruitment.betbull.data.entity.TeamEntity;
import recruitment.betbull.spring.repositories.TeamRepository;

import java.util.Currency;

import static org.hamcrest.Matchers.*;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.mockito.Mockito.*;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.put;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@SpringBootTest
@AutoConfigureMockMvc(printOnlyOnFailure = false)
@Tag(TestTags.INTEGRATION)
class TeamResourceUpdateTest {
    private static final Long teamId = 1L;
    private final ObjectMapper objectMapper;

    @MockBean
    TeamRepository teamRepository;

    @Autowired
    MockMvc mockMvc;

    @Captor
    ArgumentCaptor<TeamEntity> teamToSaveCaptor;

    TeamResourceUpdateTest() {
        objectMapper = new ObjectMapper();
        objectMapper.setVisibility(PropertyAccessor.FIELD, JsonAutoDetect.Visibility.ANY);
        objectMapper.registerModule(new JavaTimeModule());
    }

    @BeforeEach
    public void setUp() {
        reset(teamRepository);

        TeamEntity existingTeam = new TeamEntity(teamId, new Team("existingName", Currency.getInstance("USD")));
        when(teamRepository.getOne(teamId)).thenReturn(existingTeam);
        when(teamRepository.save(any())).thenAnswer(invocation -> invocation.getArgument(0));
    }

    @SneakyThrows
    @Test
    void shouldReturnValidationErrorsOnNoInput() {
        mockMvc.perform(
                put("/team/" + teamId)
                        .contentType(MediaType.APPLICATION_JSON)
        )
        .andExpect(status().isBadRequest())
        .andExpect(jsonPath("$", hasSize(1)))
        .andExpect(jsonPath("$[0]", is("update form should be present")));
    }

    @SneakyThrows
    @Test
    void shouldReturnValidationErrorsOnInvalidInput() {
        final TeamForm fullyInvalidTeam = new TeamForm(null, null);

        mockMvc.perform(
                put("/team/" + teamId)
                        .contentType(MediaType.APPLICATION_JSON)
                        .content(objectMapper.writeValueAsString(fullyInvalidTeam))
        )
        .andExpect(status().isBadRequest())
        .andExpect(jsonPath("$.name", hasSize(2)))
        .andExpect(jsonPath("$.name", containsInAnyOrder("value is expected", "field must be present")))
        .andExpect(jsonPath("$.currency", Matchers.contains("field must be present and have value")));
    }

    @SneakyThrows
    @Test
    void shouldReturnUpdatedEntityOnValidTeam() {
        final String exampleName = "exampleName";
        final Currency exampleCurrency = Currency.getInstance("PLN");
        final TeamForm exampleTeam = new TeamForm(exampleName, exampleCurrency);

        mockMvc.perform(
                put("/team/" + teamId)
                        .contentType(MediaType.APPLICATION_JSON)
                        .content(objectMapper.writeValueAsString(exampleTeam))
        )
        .andExpect(status().isOk())
        .andExpect(jsonPath("$.id", is(teamId.intValue())))
        .andExpect(jsonPath("$.name", is(exampleName)))
        .andExpect(jsonPath("$.currency", is("PLN")));

        verify(teamRepository).save(teamToSaveCaptor.capture());
        final TeamEntity capturedEntity = teamToSaveCaptor.getValue();
        assertEquals(teamId.intValue(), capturedEntity.getId());
        assertEquals(exampleName, capturedEntity.getName());
        assertEquals(exampleCurrency, capturedEntity.getCurrency());
    }
}