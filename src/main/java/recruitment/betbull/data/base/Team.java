package recruitment.betbull.data.base;

import lombok.*;
import lombok.experimental.SuperBuilder;

import java.util.Currency;

@NoArgsConstructor
@AllArgsConstructor
@Getter
@Setter
@SuperBuilder(toBuilder = true)
public class Team {
    private String name;
    private Currency currency;

    public Team(final Team other) {
        this.name = other.getName();
        this.currency = other.getCurrency();
    }
}
