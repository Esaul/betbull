package recruitment.betbull.spring.endpoints;

import com.fasterxml.jackson.annotation.JsonAutoDetect;
import com.fasterxml.jackson.annotation.PropertyAccessor;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.datatype.jsr310.JavaTimeModule;
import lombok.SneakyThrows;
import org.junit.jupiter.api.Tag;
import org.junit.jupiter.api.Test;
import org.mockito.ArgumentMatchers;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.SpyBean;
import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.MockMvc;
import recruitment.betbull.constants.TestTags;
import recruitment.betbull.data.base.Team;
import recruitment.betbull.data.dto.TeamForm;
import recruitment.betbull.data.entity.TeamEntity;
import recruitment.betbull.spring.services.TeamService;

import java.util.Currency;

import static org.hamcrest.Matchers.*;
import static org.mockito.Mockito.doReturn;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@SpringBootTest
@AutoConfigureMockMvc(printOnlyOnFailure = false)
@Tag(TestTags.INTEGRATION)
class TeamResourceCreateTest {
    final ObjectMapper objectMapper;

    @SpyBean
    TeamService teamService;

    @Autowired
    MockMvc mockMvc;

    TeamResourceCreateTest() {
        objectMapper = new ObjectMapper();
        objectMapper.setVisibility(PropertyAccessor.FIELD, JsonAutoDetect.Visibility.ANY);
        objectMapper.registerModule(new JavaTimeModule());
    }

    @SneakyThrows
    @Test
    void shouldReturnValidationErrorsOnNoInput() {
        mockMvc.perform(
                post("/team")
                        .contentType(MediaType.APPLICATION_JSON)
        )
        .andExpect(status().isBadRequest())
        .andExpect(jsonPath("$", hasSize(1)))
        .andExpect(jsonPath("$[0]", is("create form should be present")));
    }

    @SneakyThrows
    @Test
    void shouldReturnValidationErrorsOnInvalidInput() {
        final TeamForm fullyInvalidTeam = new TeamForm(null, null);

        mockMvc.perform(
                post("/team")
                        .contentType(MediaType.APPLICATION_JSON)
                        .content(objectMapper.writeValueAsString(fullyInvalidTeam))
        )
        .andExpect(status().isBadRequest())
        .andExpect(jsonPath("$.name", hasSize(2)))
        .andExpect(jsonPath("$.name", containsInAnyOrder("value is expected", "field must be present")))
        .andExpect(jsonPath("$.currency", hasSize(1)))
        .andExpect(jsonPath("$.currency", contains("field must be present and have value")));
    }

    @SneakyThrows
    @Test
    void shouldReturnSavedEntityOnValidTeam() {
        final String exampleName = "exampleName";
        final Currency exampleCurrency = Currency.getInstance("PLN");
        final TeamForm exampleTeam = new TeamForm(exampleName, exampleCurrency);

        mockMvc.perform(
                post("/team")
                        .contentType(MediaType.APPLICATION_JSON)
                        .content(objectMapper.writeValueAsString(exampleTeam))
        )
        .andExpect(status().isCreated())
        .andExpect(jsonPath("$.id", isA(Number.class)))
        .andExpect(jsonPath("$.name", is(exampleName)))
        .andExpect(jsonPath("$.currency", is(exampleCurrency.getCurrencyCode())));
    }

    @SneakyThrows
    @Test
    void shouldReturnSavedEntityOnValidTeamForHighIdValue() {
        final String exampleName = "exampleName";
        final Currency exampleCurrency = Currency.getInstance("PLN");
        final TeamForm exampleTeam = new TeamForm(exampleName, exampleCurrency);
        final TeamEntity exampleTeamWithHighId = new TeamEntity(Long.MAX_VALUE, exampleTeam.toTeam());

        doReturn(exampleTeamWithHighId).when(teamService).create(ArgumentMatchers.any(Team.class));

        mockMvc.perform(
                post("/team")
                        .contentType(MediaType.APPLICATION_JSON)
                        .content(objectMapper.writeValueAsString(exampleTeam))
        )
        .andExpect(status().isCreated())
        .andExpect(jsonPath("$.id", isA(Number.class)))
        .andExpect(jsonPath("$.name", is(exampleName)))
        .andExpect(jsonPath("$.currency", is(exampleCurrency.getCurrencyCode())));
    }
}