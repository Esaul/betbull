package recruitment.betbull.database;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Tag;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.util.CollectionUtils;
import recruitment.betbull.constants.TestTags;
import recruitment.betbull.data.base.Player;
import recruitment.betbull.data.base.Team;
import recruitment.betbull.data.entity.PlayerEntity;
import recruitment.betbull.data.entity.PlayerTeamEntity;
import recruitment.betbull.data.entity.TeamEntity;
import recruitment.betbull.data.entity.composites.PlayerTeamId;
import recruitment.betbull.exceptions.PlayerDoesNotExistException;
import recruitment.betbull.spring.services.PlayerService;
import recruitment.betbull.spring.services.TeamService;

import javax.persistence.EntityManager;
import javax.transaction.Transactional;
import java.time.LocalDate;
import java.util.Currency;
import java.util.HashSet;
import java.util.Objects;
import java.util.Set;

import static org.junit.jupiter.api.Assertions.*;

@SpringBootTest
@Tag(TestTags.DATABASE)
class DatabasePlayerCRUDTest {
    private static final LocalDate exampleDateFromPast = LocalDate.of(2020, 1, 1);

    private PlayerEntity validPlayer;
    private TeamEntity validTeam;

    @Autowired
    private PlayerService playerService;

    @Autowired
    private TeamService teamService;

    @Autowired
    @Qualifier("entityManager")
    private EntityManager em;

    @BeforeEach
    @Transactional(Transactional.TxType.REQUIRED)
    void cleanup() {
        final Page<PlayerEntity> allPlayers = playerService.findAll(Pageable.unpaged());
        allPlayers.stream().map(PlayerEntity::getId).forEach(playerService::delete);
        final Page<TeamEntity> allTeams = teamService.findAll(Pageable.unpaged());
        allTeams.stream().map(TeamEntity::getId).forEach(teamService::delete);

        validPlayer = new PlayerEntity(null, new Player("pName", "pSName", exampleDateFromPast));
        validTeam = new TeamEntity(null, new Team("teamName", Currency.getInstance("PLN")));
    }

    private void associatePlayerWithTeam(PlayerEntity player, TeamEntity team) {
        Set<PlayerTeamEntity> playerTeamAssociations = new HashSet<>();
        PlayerTeamEntity playerTeamAssociation =
                new PlayerTeamEntity(new PlayerTeamId(player.getId(), team.getId()), player, team, exampleDateFromPast, null);
        playerTeamAssociations.add(playerTeamAssociation);
        player.setPlayerTeams(playerTeamAssociations);
    }

    @Test
    @Transactional(Transactional.TxType.REQUIRED)
    void shouldSaveValidPlayer() {
        final PlayerEntity created = playerService.create(validPlayer);

        final PlayerEntity found = assertDoesNotThrow(() -> playerService.findById(created.getId()), "Service shouldn't throw on valid entity");

        assertEquals(validPlayer.getName(), created.getName());
        assertEquals(validPlayer.getSurname(), created.getSurname());
        assertEquals(validPlayer.getBirthdate(), created.getBirthdate());
        assertTrue(CollectionUtils.isEmpty(created.getPlayerTeams()));
        assertEquals(validPlayer.getName(), found.getName());
        assertEquals(validPlayer.getSurname(), found.getSurname());
        assertEquals(validPlayer.getBirthdate(), found.getBirthdate());
        assertTrue(CollectionUtils.isEmpty(found.getPlayerTeams()));
    }

    @Test
    @Transactional(Transactional.TxType.REQUIRED)
    void shouldSaveValidPlayerWithTeam() {
        final TeamEntity createdTeam = teamService.create(validTeam);
        associatePlayerWithTeam(validPlayer, validTeam);
        final PlayerEntity createdPlayer = playerService.create(validPlayer);

        final PlayerEntity found = assertDoesNotThrow(() -> playerService.findById(createdPlayer.getId()), "Service shouldn't throw on valid entity");

        assertEquals(createdPlayer.getName(), found.getName());
        assertEquals(createdPlayer.getSurname(), found.getSurname());
        assertEquals(createdPlayer.getBirthdate(), found.getBirthdate());
        assertFalse(CollectionUtils.isEmpty(found.getPlayerTeams()));
        final PlayerTeamEntity teamAssociation = createdPlayer.getPlayerTeams().iterator().next();
        assertEquals(exampleDateFromPast, teamAssociation.getMemberFrom());
        assertNull(teamAssociation.getMemberTo());
        assertEquals(createdTeam.getId(), teamAssociation.getTeamId().getId());
        assertEquals(createdTeam.getName(), teamAssociation.getTeamId().getName());
        assertEquals(createdTeam.getCurrency(), teamAssociation.getTeamId().getCurrency());
    }

    @Test
    @Transactional(Transactional.TxType.REQUIRED)
    void shouldUpdatePlayer() {
        final String updatedName = "updatedName";
        final String updatedSurname = "updatedSurname";
        final LocalDate updatedBirthdate = LocalDate.now();
        final PlayerEntity createdPlayer = playerService.create(validPlayer);
        em.clear();
        createdPlayer.setName(updatedName);
        createdPlayer.setSurname(updatedSurname);
        createdPlayer.setBirthdate(updatedBirthdate);

        final PlayerEntity updatedPlayer = playerService.update(createdPlayer);

        assertEquals(updatedName, updatedPlayer.getName());
        assertEquals(updatedSurname, updatedPlayer.getSurname());
        assertEquals(updatedBirthdate, updatedPlayer.getBirthdate());
    }

    @Test
    @Transactional(Transactional.TxType.REQUIRED)
    void shouldUpdateChangesToTeamsOnPlayerUpdate() {
        final PlayerEntity createdPlayer = playerService.create(validPlayer);
        final TeamEntity createdTeam = teamService.create(validTeam);
        em.clear();
        associatePlayerWithTeam(createdPlayer, createdTeam);

        final PlayerEntity updatedPlayer = playerService.update(createdPlayer);

        assertEquals(1, updatedPlayer.getPlayerTeams().size());
        assertTrue(
                updatedPlayer.getPlayerTeams().stream()
                        .anyMatch(pt -> createdTeam.getId().equals(pt.getTeamId().getId())
                                && exampleDateFromPast.equals(pt.getMemberFrom())
                                && Objects.isNull(pt.getMemberTo())),
                "Player teams should have updated team"
        );
    }

    @Test
    @Transactional(Transactional.TxType.REQUIRED)
    void shouldAddTeamToPlayer() {
        final TeamEntity createdTeam = teamService.create(validTeam);
        final PlayerEntity createdPlayer = playerService.create(validPlayer);

        final PlayerEntity found = assertDoesNotThrow(() -> playerService.addTeam(createdPlayer.getId(), createdTeam.getId()));

        final PlayerTeamEntity teamAssociation = found.getPlayerTeams().iterator().next();
        assertEquals(LocalDate.now(), teamAssociation.getMemberFrom());
        assertNull(teamAssociation.getMemberTo());
        assertEquals(createdTeam.getId(), teamAssociation.getPlayerTeamId().getTeamId());
        assertEquals(createdPlayer.getId(), teamAssociation.getPlayerTeamId().getPlayerId());
        assertEquals(createdTeam.getId(), teamAssociation.getTeamId().getId());
        assertEquals(createdTeam.getName(), teamAssociation.getTeamId().getName());
        assertEquals(createdTeam.getCurrency(), teamAssociation.getTeamId().getCurrency());
        assertEquals(createdPlayer.getId(), teamAssociation.getPlayerId().getId());
        assertEquals(createdPlayer.getName(), teamAssociation.getPlayerId().getName());
        assertEquals(createdPlayer.getSurname(), teamAssociation.getPlayerId().getSurname());
        assertEquals(createdPlayer.getBirthdate(), teamAssociation.getPlayerId().getBirthdate());
    }

    @Test
    @Transactional(Transactional.TxType.REQUIRED)
    void shouldDeleteExistingPlayer() {
        final PlayerEntity createdPlayer = playerService.create(validPlayer);
        em.clear();
        final Long playerId = createdPlayer.getId();
        playerService.delete(playerId);

        assertThrows(PlayerDoesNotExistException.class, () -> playerService.findById(playerId), "Entity shouldn't be available after deletion");
    }
}