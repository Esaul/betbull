package recruitment.betbull.data.base;

import lombok.AllArgsConstructor;
import lombok.Getter;
import org.springframework.data.domain.Page;
import recruitment.betbull.data.entity.TeamEntity;

import java.util.List;
import java.util.stream.Collectors;

@AllArgsConstructor
@Getter
public class TeamsPage {
    private final List<Team> teams;
    private final int page;
    private final int elementsPerPage;
    private final long total;

    public TeamsPage(Page<TeamEntity> teamPage) {
        this(teamPage.getContent().stream().map(Team::new).collect(Collectors.toList()),
            teamPage.getPageable().getPageNumber(),
            teamPage.getPageable().getPageSize(),
            teamPage.getTotalElements()
        );
    }
}
