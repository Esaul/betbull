package recruitment.betbull.spring.services;

import lombok.AllArgsConstructor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import recruitment.betbull.data.base.Player;
import recruitment.betbull.data.dto.PlayerTransferResult;
import recruitment.betbull.data.entity.PlayerEntity;
import recruitment.betbull.data.entity.PlayerTeamEntity;
import recruitment.betbull.data.entity.TeamEntity;
import recruitment.betbull.data.entity.composites.PlayerTeamId;
import recruitment.betbull.exceptions.PlayerDoesNotExistException;
import recruitment.betbull.exceptions.TeamNotFoundException;
import recruitment.betbull.spring.repositories.PlayerRepository;

import javax.transaction.Transactional;
import java.time.LocalDate;
import java.util.Optional;


@Service
@AllArgsConstructor(onConstructor = @__(@Autowired))
public class PlayerService {
    final PlayerRepository playerRepository;
    final TeamService teamService;

    @Transactional(Transactional.TxType.REQUIRED)
    public Page<PlayerEntity> findAll(final Pageable page) {
        return playerRepository.findAll(page);
    }

    @Transactional(Transactional.TxType.REQUIRED)
    public PlayerEntity findById(final Long id) {
        return playerRepository.findById(id).orElseThrow(() -> new PlayerDoesNotExistException(id));
    }

    @Transactional(Transactional.TxType.REQUIRED)
    public PlayerEntity create(final Player player) {
        final PlayerEntity playerEntity = new PlayerEntity(player);
        return create(playerEntity);
    }

    @Transactional(Transactional.TxType.REQUIRED)
    public PlayerEntity create(final PlayerEntity player) {
        return playerRepository.save(player);
    }

    @Transactional(Transactional.TxType.REQUIRED)
    public PlayerEntity update(final Long id, final Player player) {
        final PlayerEntity playerEntity = new PlayerEntity(id, player);
        return update(playerEntity);
    }

    @Transactional(Transactional.TxType.REQUIRED)
    public PlayerEntity update(PlayerEntity playerEntity) {
        return playerRepository.save(playerEntity);
    }

    @Transactional(Transactional.TxType.REQUIRED)
    public PlayerEntity addTeam(final Long playerId, final Long teamId) {
        final PlayerEntity playerToUpdate = playerRepository.findById(playerId).orElseThrow(() -> new PlayerDoesNotExistException(playerId));
        final TeamEntity teamToAdd = teamService.findById(teamId);
        final PlayerTeamEntity playerTeamToAdd = new PlayerTeamEntity(
                new PlayerTeamId(playerId, teamId), playerToUpdate, teamToAdd, LocalDate.now(), null
        );
        playerToUpdate.getPlayerTeams().add(playerTeamToAdd);
        return playerRepository.save(playerToUpdate);
    }

    @Transactional(Transactional.TxType.REQUIRED)
    public PlayerTransferResult transferTeam(Long playerId, Long teamId) {
        final PlayerEntity playerToUpdate = playerRepository.findById(playerId).orElseThrow(() -> new PlayerDoesNotExistException(playerId));
        final TeamEntity teamToAdd = teamService.findById(teamId);
        final PlayerTeamEntity playerTeamToAdd = new PlayerTeamEntity(
                new PlayerTeamId(playerId, teamId), playerToUpdate, teamToAdd, LocalDate.now(), null
        );

        final Optional<PlayerTeamEntity> previousTeam = findPreviousTeam(playerToUpdate);
        previousTeam.ifPresent(playerTeam -> playerTeam.setMemberTo(LocalDate.now()));

        playerToUpdate.getPlayerTeams().add(playerTeamToAdd);
        final PlayerEntity updatedPlayer = playerRepository.save(playerToUpdate);
        return generateResult(previousTeam, updatedPlayer);
    }

    private Optional<PlayerTeamEntity> findPreviousTeam(PlayerEntity playerToUpdate) {
        return playerToUpdate.getPlayerTeams()
                .stream()
                .filter(playerTeam -> playerTeam.getMemberTo() == null)
                .findFirst();
    }

    private PlayerTransferResult generateResult(Optional<PlayerTeamEntity> previousTeam, PlayerEntity updatedPlayer) {
        return previousTeam
                .map(prevTeam -> new PlayerTransferResult(updatedPlayer, prevTeam))
                .orElseGet(() -> new PlayerTransferResult(updatedPlayer));
    }

    @Transactional(Transactional.TxType.REQUIRED)
    public Player removeTeam(final Long playerId, final Long teamId) {
        final PlayerEntity playerToUpdate = playerRepository.findById(playerId).orElseThrow(() -> new PlayerDoesNotExistException(playerId));
        final boolean wasRemoved = playerToUpdate.getPlayerTeams().removeIf(team -> teamId.equals(team.getTeamId().getId()));
        if(!wasRemoved) {
            throw new TeamNotFoundException(playerId, teamId);
        }
        return playerRepository.save(playerToUpdate);
    }

    @Transactional(Transactional.TxType.REQUIRED)
    public void delete(final Long id) {
        playerRepository.deleteById(id);
    }
}
