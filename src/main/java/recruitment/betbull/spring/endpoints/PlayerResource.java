package recruitment.betbull.spring.endpoints;

import lombok.AllArgsConstructor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpStatus;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;
import recruitment.betbull.data.base.Player;
import recruitment.betbull.data.base.PlayersPage;
import recruitment.betbull.data.dto.PlayerForm;
import recruitment.betbull.data.dto.PlayerTransferResult;
import recruitment.betbull.data.entity.PlayerEntity;
import recruitment.betbull.spring.services.PlayerService;

import javax.validation.Valid;
import javax.validation.constraints.Min;
import javax.validation.constraints.NotNull;

@RestController
@RequestMapping("/player")
@Validated
@AllArgsConstructor(onConstructor = @__(@Autowired))
public class PlayerResource {
    final PlayerService playerService;

    @GetMapping("/all/{page}")
    public PlayersPage getAll(
            @PathVariable(value = "page")
            @Min(value = 0, message = "page number must be present not negative number")
            @NotNull
            final Integer pageNumber) {
        final Pageable page = PageRequest.of(pageNumber, 20);
        final Page<PlayerEntity> playerPage = playerService.findAll(page);
        return new PlayersPage(playerPage);
    }

    @GetMapping("/{id}")
    public PlayerEntity get(@PathVariable("id") final Long id) {
        return playerService.findById(id);
    }

    @PostMapping
    @ResponseStatus(HttpStatus.CREATED)
    public Player create(
            @RequestBody(required = false)
            @NotNull(message = "create form should be present")
            @Valid
            final PlayerForm form) {
        return playerService.create(form.toPlayer());
    }

    @PutMapping("/{id}")
    public Player update(@PathVariable("id") final Long id,
            @RequestBody(required = false)
            @NotNull(message = "update form should be present")
            @Valid
            final PlayerForm form) {
        return playerService.update(id, form.toPlayer());
    }

    @PutMapping("/addTeam")
    public Player addTeam(
            @RequestParam(value = "playerId", required = false)
            @Min(value = 1, message = "id must be a positive number")
            @NotNull(message = "player ID is required")
            @Valid
            final Long playerId,
            @RequestParam(value = "teamId", required = false)
            @Min(value = 1, message = "id must be a positive number")
            @NotNull(message = "team ID is required")
            @Valid
            final Long teamId) {
        return playerService.addTeam(playerId, teamId);
    }

    @PutMapping("/transfer")
    public PlayerTransferResult transfer(
            @RequestParam(value = "playerId", required = false)
            @Min(value = 1, message = "id must be a positive number")
            @NotNull(message = "player ID is required")
            @Valid
            final Long playerId,
            @RequestParam(value = "newTeamId", required = false)
            @Min(value = 1, message = "id must be a positive number")
            @NotNull(message = "new team ID is required")
            @Valid
            final Long teamId) {
        return playerService.transferTeam(playerId, teamId);
    }

    @DeleteMapping("/removeTeam")
    public Player removeTeam(
            @RequestParam(value = "playerId", required = false)
            @Min(value = 1, message = "id must be a positive number")
            @NotNull(message = "player ID is required")
            @Valid
            final Long playerId,
            @RequestParam(value = "teamId", required = false)
            @Min(value = 1, message = "id must be a positive number")
            @NotNull(message = "team ID is required")
            @Valid
            final Long teamId) {
        return playerService.removeTeam(playerId, teamId);
    }

    @DeleteMapping("/{id}")
    public void delete(@PathVariable("id") final Long id) {
        playerService.delete(id);
    }
}
