package recruitment.betbull.data.entity;

import com.fasterxml.jackson.annotation.JsonIgnore;
import lombok.*;
import recruitment.betbull.data.base.Team;

import javax.persistence.*;
import java.util.Currency;
import java.util.Set;

@NoArgsConstructor
@AllArgsConstructor
@Setter
@Builder(builderMethodName = "entityBuilder")
@Entity(name = "team")
public class TeamEntity extends Team {
    private Long id;
    private Set<PlayerTeamEntity> playerTeams;

    public TeamEntity(final Team base) {
        super(base);
    }

    public TeamEntity(final Long id, final Team base) {
        this(base);
        this.id = id;
    }

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    public Long getId() {
        return id;
    }

    @JsonIgnore
    @OneToMany(mappedBy = "teamId", orphanRemoval = true, cascade = CascadeType.ALL)
    public Set<PlayerTeamEntity> getPlayerTeams() {
        return playerTeams;
    }

    @Override
    @Column(nullable = false)
    public Currency getCurrency() {
        return super.getCurrency();
    }

    @Override
    @Column(nullable = false)
    public String getName() {
        return super.getName();
    }
}
