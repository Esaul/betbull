package recruitment.betbull.data.entity;

import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import lombok.*;
import lombok.experimental.SuperBuilder;
import recruitment.betbull.data.base.Player;
import recruitment.betbull.data.serializers.PlayerTeamTeamsSerializer;

import javax.persistence.*;
import java.time.LocalDate;
import java.util.HashSet;
import java.util.Set;

@NoArgsConstructor
@Setter
@SuperBuilder(toBuilder = true)
@Entity(name = "player")
public class PlayerEntity extends Player {
    private Long id;
    private Set<PlayerTeamEntity> playerTeams = new HashSet<>();

    public PlayerEntity(final Player base) {
        super(base);
    }

    public PlayerEntity(final Long id, final Player base) {
        this(base);
        this.id = id;
    }

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    public Long getId() {
        return id;
    }

    @JsonSerialize(using = PlayerTeamTeamsSerializer.class)
    @OneToMany(mappedBy = "playerId", orphanRemoval = true, cascade = CascadeType.ALL)
    public Set<PlayerTeamEntity> getPlayerTeams() {
        return playerTeams;
    }

    @Override
    @Column(nullable = false)
    public LocalDate getBirthdate() {
        return super.getBirthdate();
    }

    @Override
    @Column(nullable = false)
    public String getName() {
        return super.getName();
    }

    @Override
    @Column(nullable = false)
    public String getSurname() {
        return super.getSurname();
    }
}


