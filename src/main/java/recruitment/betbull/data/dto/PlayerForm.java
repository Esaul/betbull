package recruitment.betbull.data.dto;

import lombok.AllArgsConstructor;
import recruitment.betbull.data.base.Player;

import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Past;
import java.time.LocalDate;

@AllArgsConstructor
public class PlayerForm {
    @NotNull(message = "field must be present")
    @NotEmpty(message = "value is expected")
    private String name;
    @NotNull(message = "field must be present")
    @NotEmpty(message = "value is expected")
    private String surname;
    @NotNull(message = "field must be present and have value")
    @Past(message = "must be from the past")
    private LocalDate birthdate;

    public Player toPlayer() {
        return new Player(name, surname, birthdate);
    }
}
