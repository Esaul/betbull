package recruitment.betbull.data.base;

import lombok.AllArgsConstructor;
import lombok.Getter;
import org.springframework.data.domain.Page;
import recruitment.betbull.data.entity.PlayerEntity;

import java.util.List;
import java.util.stream.Collectors;

@AllArgsConstructor
@Getter
public class PlayersPage {
    private final List<Player> players;
    private final int page;
    private final int elementsPerPage;
    private final long total;

    public PlayersPage(Page<PlayerEntity> playerPage) {
        this(playerPage.getContent().stream().map(Player::new).collect(Collectors.toList()),
            playerPage.getPageable().getPageNumber(),
            playerPage.getPageable().getPageSize(),
            playerPage.getTotalElements()
        );
    }
}
