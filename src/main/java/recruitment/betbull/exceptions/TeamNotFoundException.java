package recruitment.betbull.exceptions;

import lombok.AllArgsConstructor;

@AllArgsConstructor
public class TeamNotFoundException extends RuntimeException {
    private final Long playerId;
    private final Long teamId;

    @Override
    public String getMessage() {
        return "Player with id " + playerId + " does not have team with id " + teamId;
    }
}
