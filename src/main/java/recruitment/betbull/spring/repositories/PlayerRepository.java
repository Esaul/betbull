package recruitment.betbull.spring.repositories;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;
import recruitment.betbull.data.entity.PlayerEntity;


@Repository
public interface PlayerRepository extends JpaRepository<PlayerEntity, Long> {
}
