package recruitment.betbull.spring.endpoints;

import com.fasterxml.jackson.annotation.JsonAutoDetect;
import com.fasterxml.jackson.annotation.PropertyAccessor;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.datatype.jsr310.JavaTimeModule;
import lombok.SneakyThrows;
import org.junit.jupiter.api.Tag;
import org.junit.jupiter.api.Test;
import org.mockito.ArgumentMatchers;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.SpyBean;
import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.MockMvc;
import recruitment.betbull.constants.TestTags;
import recruitment.betbull.data.base.Player;
import recruitment.betbull.data.dto.PlayerForm;
import recruitment.betbull.data.entity.PlayerEntity;
import recruitment.betbull.spring.services.PlayerService;

import java.time.LocalDate;

import static org.hamcrest.Matchers.*;
import static org.mockito.Mockito.doReturn;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@SpringBootTest
@AutoConfigureMockMvc(printOnlyOnFailure = false)
@Tag(TestTags.INTEGRATION)
class PlayerResourceCreateTest {
    final ObjectMapper objectMapper;

    @SpyBean
    PlayerService playerService;

    @Autowired
    MockMvc mockMvc;

    PlayerResourceCreateTest() {
        objectMapper = new ObjectMapper();
        objectMapper.setVisibility(PropertyAccessor.FIELD, JsonAutoDetect.Visibility.ANY);
        objectMapper.registerModule(new JavaTimeModule());
    }

    @SneakyThrows
    @Test
    void shouldReturnValidationErrorsOnNoInput() {
        mockMvc.perform(
                post("/player")
                        .contentType(MediaType.APPLICATION_JSON)
        )
        .andExpect(status().isBadRequest())
        .andExpect(jsonPath("$", hasSize(1)))
        .andExpect(jsonPath("$[0]", is("create form should be present")));
    }

    @SneakyThrows
    @Test
    void shouldReturnValidationErrorsOnInvalidInput() {
        final PlayerForm fullyInvalidPlayer = new PlayerForm(null, null, null);

        mockMvc.perform(
                post("/player")
                        .contentType(MediaType.APPLICATION_JSON)
                        .content(objectMapper.writeValueAsString(fullyInvalidPlayer))
        )
        .andExpect(status().isBadRequest())
        .andExpect(jsonPath("$.name", hasSize(2)))
        .andExpect(jsonPath("$.name", containsInAnyOrder("value is expected", "field must be present")))
        .andExpect(jsonPath("$.surname", hasSize(2)))
        .andExpect(jsonPath("$.surname", containsInAnyOrder("value is expected", "field must be present")))
        .andExpect(jsonPath("$.birthdate", hasSize(1)))
        .andExpect(jsonPath("$.birthdate", contains("field must be present and have value")));
    }

    @SneakyThrows
    @Test
    void shouldReturnSavedEntityOnValidPlayer() {
        final String exampleName = "exampleName";
        final String exampleSurname = "exampleSurname";
        final LocalDate exampleDate = LocalDate.of(2020, 1, 1);
        final PlayerForm examplePlayer = new PlayerForm(exampleName, exampleSurname, exampleDate);

        mockMvc.perform(
                post("/player")
                        .contentType(MediaType.APPLICATION_JSON)
                        .content(objectMapper.writeValueAsString(examplePlayer))
        )
        .andExpect(status().isCreated())
        .andExpect(jsonPath("$.id", isA(Number.class)))
        .andExpect(jsonPath("$.name", is(exampleName)))
        .andExpect(jsonPath("$.surname", is(exampleSurname)))
        .andExpect(jsonPath("$.birthdate", is(exampleDate.toString())))
        .andExpect(jsonPath("$.playerTeams", empty()));
    }

    @SneakyThrows
    @Test
    void shouldReturnSavedEntityOnValidTeamForHighIdValue() {
        final String exampleName = "exampleName";
        final String exampleSurname = "exampleSurname";
        final LocalDate exampleDate = LocalDate.of(2020, 1, 1);
        final PlayerForm examplePlayer = new PlayerForm(exampleName, exampleSurname, exampleDate);
        final PlayerEntity examplePlayerWithHighId = new PlayerEntity(Long.MAX_VALUE, examplePlayer.toPlayer());

        doReturn(examplePlayerWithHighId).when(playerService).create(ArgumentMatchers.any(Player.class));

        mockMvc.perform(
                        post("/player")
                                .contentType(MediaType.APPLICATION_JSON)
                                .content(objectMapper.writeValueAsString(examplePlayer))
                )
                .andExpect(status().isCreated())
                .andExpect(jsonPath("$.id", isA(Number.class)))
                .andExpect(jsonPath("$.name", is(exampleName)))
                .andExpect(jsonPath("$.surname", is(exampleSurname)))
                .andExpect(jsonPath("$.birthdate", is(exampleDate.toString())))
                .andExpect(jsonPath("$.playerTeams", empty()));
    }
}