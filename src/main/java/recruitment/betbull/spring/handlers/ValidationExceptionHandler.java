package recruitment.betbull.spring.handlers;

import org.springframework.beans.TypeMismatchException;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.FieldError;
import org.springframework.web.bind.MethodArgumentNotValidException;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;

import org.springframework.web.context.request.WebRequest;
import org.springframework.web.servlet.mvc.method.annotation.ResponseEntityExceptionHandler;
import recruitment.betbull.exceptions.PlayerDoesNotExistException;
import recruitment.betbull.exceptions.TeamDoesNotExistException;
import recruitment.betbull.exceptions.TeamNotFoundException;

import javax.validation.ConstraintViolation;
import javax.validation.ConstraintViolationException;
import java.util.*;
import java.util.stream.Collectors;

@ControllerAdvice
public class ValidationExceptionHandler extends ResponseEntityExceptionHandler {

    public Map<String, List<String>> handleValidationExceptions(final MethodArgumentNotValidException exceptions) {
        return exceptions.getAllErrors()
                .stream()
                .map(FieldError.class::cast)
                .collect(Collectors.groupingBy(FieldError::getField, Collectors.mapping(FieldError::getDefaultMessage, Collectors.toList())));
    }

    @Override
    protected ResponseEntity<Object> handleMethodArgumentNotValid(final MethodArgumentNotValidException ex, final HttpHeaders headers, final HttpStatus status, final WebRequest request) {
        final Map<String, List<String>> mappedErrors = handleValidationExceptions(ex);
        return new ResponseEntity<>(mappedErrors, HttpStatus.BAD_REQUEST);
    }

    @Override
    protected ResponseEntity<Object> handleTypeMismatch(final TypeMismatchException ex, final HttpHeaders headers, final HttpStatus status, final WebRequest request) {
        final Throwable cause = ex.getCause();
        if(cause instanceof NumberFormatException) {
            return new ResponseEntity<>("Cannot parse value to number (value: \"" + ex.getValue() +"\")", HttpStatus.BAD_REQUEST);
        }
        throw ex;
    }

    @ExceptionHandler(IllegalArgumentException.class)
    public ResponseEntity<Object> handleIllegalArgumentException(final IllegalArgumentException exception) {
        return new ResponseEntity<>(exception.getMessage(), HttpStatus.BAD_REQUEST);
    }

    @ExceptionHandler(ConstraintViolationException.class)
    public ResponseEntity<Object> handleConstraintViolationException(final ConstraintViolationException exception) {
        final Set<String> violationMessages = exception.getConstraintViolations().stream()
                .map(ConstraintViolation::getMessage)
                .collect(Collectors.toSet());
        return new ResponseEntity<>(violationMessages, HttpStatus.BAD_REQUEST);
    }

    @ExceptionHandler({
            PlayerDoesNotExistException.class,
            TeamDoesNotExistException.class,
            TeamNotFoundException.class
    })
    public ResponseEntity<Object> handleTeamNotFoundException(final Exception exception) {
        return new ResponseEntity<>(exception.getMessage(), HttpStatus.BAD_REQUEST);
    }
}
