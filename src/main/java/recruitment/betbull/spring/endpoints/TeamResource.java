package recruitment.betbull.spring.endpoints;

import lombok.AllArgsConstructor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpStatus;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;
import recruitment.betbull.data.base.Team;
import recruitment.betbull.data.base.TeamsPage;
import recruitment.betbull.data.dto.TeamForm;
import recruitment.betbull.data.entity.TeamEntity;
import recruitment.betbull.spring.services.TeamService;

import javax.validation.Valid;
import javax.validation.constraints.Min;
import javax.validation.constraints.NotNull;

@RestController
@RequestMapping("/team")
@Validated
@AllArgsConstructor(onConstructor = @__(@Autowired))
public class TeamResource {
    final TeamService teamService;

    @GetMapping("/all/{page}")
    public TeamsPage getAll(
            @PathVariable(value = "page")
            @Min(value = 0, message = "page number must be present not negative number")
            @NotNull
            final Integer pageNumber) {
        final Pageable page = PageRequest.of(pageNumber, 20);
        final Page<TeamEntity> teamPage = teamService.findAll(page);
        return new TeamsPage(teamPage);
    }

    @GetMapping("/{id}")
    public Team getTeam(@PathVariable("id") final Long id) {
        return teamService.findById(id);
    }

    @PostMapping
    @ResponseStatus(HttpStatus.CREATED)
    public Team create(
            @RequestBody(required = false)
            @NotNull(message = "create form should be present")
            @Valid
            final TeamForm team) {
        return teamService.create(team.toTeam());
    }

    @PutMapping("/{id}")
    public Team update(@PathVariable("id") final Long id,
            @RequestBody(required = false)
            @NotNull(message = "update form should be present")
            @Valid
            final TeamForm form) {
        return teamService.update(id, form.toTeam());
    }

    @DeleteMapping("/{id}")
    public void delete(@PathVariable("id") final Long id) {
        teamService.delete(id);
    }
}
