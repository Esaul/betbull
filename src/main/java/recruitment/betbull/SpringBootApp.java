package recruitment.betbull;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication(scanBasePackages = {
        "recruitment.betbull.spring.*"
})
public class SpringBootApp {
    public static void main(final String[] args) {
        SpringApplication.run(SpringBootApp.class);
    }
}
