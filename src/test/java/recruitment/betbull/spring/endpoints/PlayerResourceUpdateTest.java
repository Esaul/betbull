package recruitment.betbull.spring.endpoints;

import com.fasterxml.jackson.annotation.JsonAutoDetect;
import com.fasterxml.jackson.annotation.PropertyAccessor;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.datatype.jsr310.JavaTimeModule;
import lombok.SneakyThrows;
import org.hamcrest.Matchers;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Tag;
import org.junit.jupiter.api.Test;
import org.mockito.ArgumentCaptor;
import org.mockito.Captor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.MockMvc;
import recruitment.betbull.constants.TestTags;
import recruitment.betbull.data.base.Player;
import recruitment.betbull.data.dto.PlayerForm;
import recruitment.betbull.data.entity.PlayerEntity;
import recruitment.betbull.spring.repositories.PlayerRepository;

import java.time.LocalDate;

import static org.hamcrest.Matchers.*;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertTrue;
import static org.mockito.Mockito.*;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.put;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@SpringBootTest
@AutoConfigureMockMvc(printOnlyOnFailure = false)
@Tag(TestTags.INTEGRATION)
class PlayerResourceUpdateTest {
    private static final Long playerId = 1L;
    private final ObjectMapper objectMapper;

    @MockBean
    PlayerRepository playerRepository;

    @Autowired
    MockMvc mockMvc;

    @Captor
    ArgumentCaptor<PlayerEntity> playerToSaveCaptor;

    PlayerResourceUpdateTest() {
        objectMapper = new ObjectMapper();
        objectMapper.setVisibility(PropertyAccessor.FIELD, JsonAutoDetect.Visibility.ANY);
        objectMapper.registerModule(new JavaTimeModule());
    }

    @BeforeEach
    public void setUp() {
        reset(playerRepository);

        PlayerEntity existingPlayer = new PlayerEntity(playerId, new Player("existingName", "existingSurname", LocalDate.of(2000, 10, 10)));
        when(playerRepository.getOne(playerId)).thenReturn(existingPlayer);
        when(playerRepository.save(any())).thenAnswer(invocation -> invocation.getArgument(0));
    }

    @SneakyThrows
    @Test
    void shouldReturnValidationErrorsOnNoInput() {
        mockMvc.perform(
                put("/player/" + playerId)
                        .contentType(MediaType.APPLICATION_JSON)
        )
        .andExpect(status().isBadRequest())
        .andExpect(jsonPath("$", hasSize(1)))
        .andExpect(jsonPath("$[0]", is("update form should be present")));
    }

    @SneakyThrows
    @Test
    void shouldReturnValidationErrorsOnInvalidInput() {
        final PlayerForm fullyInvalidPlayer = new PlayerForm(null, null, null);

        mockMvc.perform(
                put("/player/" + playerId)
                        .contentType(MediaType.APPLICATION_JSON)
                        .content(objectMapper.writeValueAsString(fullyInvalidPlayer))
        )
        .andExpect(status().isBadRequest())
        .andExpect(jsonPath("$.name", hasSize(2)))
        .andExpect(jsonPath("$.name", containsInAnyOrder("value is expected", "field must be present")))
        .andExpect(jsonPath("$.surname", hasSize(2)))
        .andExpect(jsonPath("$.surname", containsInAnyOrder("value is expected", "field must be present")))
        .andExpect(jsonPath("$.birthdate", hasSize(1)))
        .andExpect(jsonPath("$.birthdate", Matchers.contains("field must be present and have value")));
    }

    @SneakyThrows
    @Test
    void shouldReturnUpdatedEntityOnValidPlayer() {
        final String exampleName = "exampleName";
        final String exampleSurname = "exampleSurname";
        final LocalDate exampleDate = LocalDate.of(2020, 1, 1);
        final PlayerForm examplePlayer = new PlayerForm(exampleName, exampleSurname, exampleDate);

        mockMvc.perform(
                put("/player/" + playerId)
                        .contentType(MediaType.APPLICATION_JSON)
                        .content(objectMapper.writeValueAsString(examplePlayer))
        )
        .andExpect(status().isOk())
        .andExpect(jsonPath("$.id", is(playerId.intValue())))
        .andExpect(jsonPath("$.name", is(exampleName)))
        .andExpect(jsonPath("$.surname", is(exampleSurname)))
        .andExpect(jsonPath("$.birthdate", is(exampleDate.toString())))
        .andExpect(jsonPath("$.playerTeams", empty()));

        verify(playerRepository).save(playerToSaveCaptor.capture());
        final PlayerEntity capturedEntity = playerToSaveCaptor.getValue();
        assertEquals(playerId.intValue(), capturedEntity.getId());
        assertEquals(exampleName, capturedEntity.getName());
        assertEquals(exampleSurname, capturedEntity.getSurname());
        assertEquals(exampleDate, capturedEntity.getBirthdate());
        assertTrue(capturedEntity.getPlayerTeams().isEmpty());
    }
}