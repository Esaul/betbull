package recruitment.betbull.spring.endpoints;

import lombok.SneakyThrows;
import org.junit.jupiter.api.Tag;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.PageRequest;
import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.MockMvc;
import recruitment.betbull.constants.TestTags;
import recruitment.betbull.data.base.Player;
import recruitment.betbull.data.base.Team;
import recruitment.betbull.data.entity.PlayerEntity;
import recruitment.betbull.data.entity.PlayerTeamEntity;
import recruitment.betbull.data.entity.TeamEntity;
import recruitment.betbull.data.entity.composites.PlayerTeamId;
import recruitment.betbull.spring.repositories.PlayerRepository;

import java.time.LocalDate;
import java.util.Currency;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;
import java.util.stream.IntStream;

import static org.hamcrest.Matchers.*;
import static org.mockito.Mockito.when;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@SpringBootTest
@AutoConfigureMockMvc(printOnlyOnFailure = false)
@Tag(TestTags.INTEGRATION)
class PlayerResourceListTest {
    @MockBean
    PlayerRepository playerRepository;

    @Autowired
    MockMvc mockMvc;

    private List<PlayerEntity> generatePlayers(int amount) {
        return IntStream.range(0, amount)
            .mapToObj(id -> new PlayerEntity((long) id, new Player("exampleName" + id, "exampleSurname" + id, LocalDate.ofYearDay(2000, id + 1))))
            .collect(Collectors.toList());
    }

    @SneakyThrows
    @Test
    void shouldReturnPageOfPlayersOnValidPageNumber() {
        final int pageNumber = 0;
        final List<PlayerEntity> existingPlayers = generatePlayers(3);
        final PageRequest requestedPage = PageRequest.of(pageNumber, 20);
        final Page<PlayerEntity> page = new PageImpl<>(existingPlayers, requestedPage, 3);

        when(playerRepository.findAll(requestedPage)).thenReturn(page);

        mockMvc.perform(
                get("/player/all/" + pageNumber)
                        .contentType(MediaType.APPLICATION_JSON)
        )
        .andExpect(status().isOk())
        .andExpect(jsonPath("$.players", hasSize(3)))
        .andExpect(jsonPath("$.page", is(pageNumber)))
        .andExpect(jsonPath("$.elementsPerPage", is(20)))
        .andExpect(jsonPath("$.total", is(3)));
    }

    @SneakyThrows
    @Test
    void shouldReturnPlayersWithoutFetchingTeams() {
        final int pageNumber = 0;
        final List<PlayerEntity> existingPlayers = generatePlayers(1);
        final PageRequest requestedPage = PageRequest.of(pageNumber, 20);
        final Page<PlayerEntity> page = new PageImpl<>(existingPlayers, requestedPage, 3);

        when(playerRepository.findAll(requestedPage)).thenReturn(page);

        mockMvc.perform(
                get("/player/all/" + pageNumber)
                        .contentType(MediaType.APPLICATION_JSON)
        )
        .andExpect(status().isOk())
        .andExpect(jsonPath("$.players", hasSize(1)))
        .andExpect(jsonPath("$.players[0].teams").doesNotExist());
    }

    @SneakyThrows
    @Test
    void shouldReturnPlayerWithTeams() {
        final Long playerId = 1L;
        final long teamId = 1L;
        final PlayerEntity existingPlayer = generatePlayers(1).get(0);
        final String teamName = "exampleTeam";
        final Currency teamCurrency = Currency.getInstance("PLN");
        final TeamEntity existingTeam = new TeamEntity(teamId, new Team(teamName, teamCurrency));
        PlayerTeamEntity existingPlayerTeamAssociation = new PlayerTeamEntity(
                new PlayerTeamId(playerId, teamId), existingPlayer, existingTeam, LocalDate.now(), null
        );
        existingPlayer.getPlayerTeams().add(existingPlayerTeamAssociation);

        when(playerRepository.findById(playerId)).thenReturn(Optional.of(existingPlayer));

        mockMvc.perform(
                get("/player/" + playerId)
                        .contentType(MediaType.APPLICATION_JSON)
        )
        .andExpect(status().isOk())
        .andExpect(jsonPath("$.name").isNotEmpty())
        .andExpect(jsonPath("$.surname").isNotEmpty())
        .andExpect(jsonPath("$.birthdate").isNotEmpty())
        .andExpect(jsonPath("$.id").isNumber())
        .andExpect(jsonPath("$.playerTeams", hasSize(1)))
        .andExpect(jsonPath("$.playerTeams[0].team.id", is((int) teamId)))
        .andExpect(jsonPath("$.playerTeams[0].team.name", is(teamName)))
        .andExpect(jsonPath("$.playerTeams[0].team.currency", is(teamCurrency.getCurrencyCode())))
        .andExpect(jsonPath("$.playerTeams[0].memberFrom", is(LocalDate.now().toString())))
        .andExpect(jsonPath("$.playerTeams[0].memberTo").doesNotExist());
    }

    @SneakyThrows
    @Test
    void shouldReturnErrorResponseOnNonExistingPlayerId() {
        final long invalidPlayerId = 10L;
        when(playerRepository.findById(invalidPlayerId)).thenReturn(Optional.empty());

        mockMvc.perform(
                        get("/player/" + invalidPlayerId)
                                .contentType(MediaType.APPLICATION_JSON)
                )
                .andExpect(status().isBadRequest())
                .andExpect(jsonPath("$", is("Player with id " + invalidPlayerId + " does not exist")));
    }

    @SneakyThrows
    @Test
    void shouldReturnErrorResponseOnNegativePageNumber() {
        final int pageNumber = -1;

        mockMvc.perform(
                get("/player/all/" + pageNumber)
                        .contentType(MediaType.APPLICATION_JSON)
        )
        .andExpect(status().isBadRequest())
        .andExpect(jsonPath("$", hasSize(1)))
        .andExpect(jsonPath("$", contains("page number must be present not negative number")));
    }

    @SneakyThrows
    @Test
    void shouldReturnErrorResponseOnInvalidPageNumber() {
        final String invalidNumber = "invalid";

        mockMvc.perform(
                get("/player/all/" + invalidNumber)
                        .contentType(MediaType.APPLICATION_JSON)
        )
        .andExpect(status().isBadRequest())
        .andExpect(jsonPath("$", is("Cannot parse value to number (value: \"" + invalidNumber + "\")")));
    }
}