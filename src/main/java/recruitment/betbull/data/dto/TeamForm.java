package recruitment.betbull.data.dto;

import lombok.AllArgsConstructor;
import recruitment.betbull.data.base.Team;

import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;
import java.util.Currency;

@AllArgsConstructor
public class TeamForm {
    @NotNull(message = "field must be present")
    @NotEmpty(message = "value is expected")
    private String name;
    @NotNull(message = "field must be present and have value")
    private Currency currency;

    public Team toTeam() {
        return new Team(name, currency);
    }
}
