package recruitment.betbull.database;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Tag;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import recruitment.betbull.constants.TestTags;
import recruitment.betbull.data.base.Player;
import recruitment.betbull.data.base.Team;
import recruitment.betbull.data.dto.PlayerTransferResult;
import recruitment.betbull.data.entity.PlayerEntity;
import recruitment.betbull.data.entity.PlayerTeamEntity;
import recruitment.betbull.data.entity.TeamEntity;
import recruitment.betbull.data.entity.composites.PlayerTeamId;
import recruitment.betbull.spring.services.PlayerService;
import recruitment.betbull.spring.services.TeamService;

import javax.persistence.EntityManager;
import javax.transaction.Transactional;
import java.math.BigDecimal;
import java.math.RoundingMode;
import java.time.LocalDate;
import java.time.temporal.ChronoUnit;
import java.util.Currency;
import java.util.HashSet;
import java.util.Objects;
import java.util.Set;

import static org.junit.jupiter.api.Assertions.*;

@SpringBootTest
@Tag(TestTags.DATABASE)
class DatabaseTest {
    private static final LocalDate exampleDateFromPast = LocalDate.of(2020, 1, 1);

    private PlayerEntity validPlayer;
    private TeamEntity validTeam;
    private TeamEntity validTeam2;

    @Autowired
    private PlayerService playerService;

    @Autowired
    private TeamService teamService;

    @Autowired
    @Qualifier("entityManager")
    private EntityManager em;

    @BeforeEach
    @Transactional(Transactional.TxType.REQUIRED)
    void cleanup() {
        final Page<PlayerEntity> allPlayers = playerService.findAll(Pageable.unpaged());
        allPlayers.stream().map(PlayerEntity::getId).forEach(playerService::delete);
        final Page<TeamEntity> allTeams = teamService.findAll(Pageable.unpaged());
        allTeams.stream().map(TeamEntity::getId).forEach(teamService::delete);

        validPlayer = new PlayerEntity(null, new Player("pName", "pSName", exampleDateFromPast));
        validTeam = new TeamEntity(null, new Team("teamName", Currency.getInstance("PLN")));
        validTeam2 = new TeamEntity(null, new Team("teamName2", Currency.getInstance("EUR")));
    }

    private void associatePlayerWithTeam(PlayerEntity player, TeamEntity team) {
        Set<PlayerTeamEntity> playerTeamAssociations = new HashSet<>();
        PlayerTeamEntity playerTeamAssociation =
                new PlayerTeamEntity(new PlayerTeamId(player.getId(), team.getId()), player, team, exampleDateFromPast, null);
        playerTeamAssociations.add(playerTeamAssociation);
        player.setPlayerTeams(playerTeamAssociations);
    }

    @Test
    @Transactional(Transactional.TxType.REQUIRED)
    void shouldAddNewTeamOnTransfer() {
        final TeamEntity createdTeam = teamService.create(validTeam);
        final PlayerEntity createdPlayer = playerService.create(validPlayer);
        em.clear();

        final PlayerTransferResult transferResult = assertDoesNotThrow(() -> playerService.transferTeam(createdPlayer.getId(), createdTeam.getId()));

        assertEquals(createdPlayer.getId(), transferResult.getPlayer().getId());
        assertEquals(createdPlayer.getName(), transferResult.getPlayer().getName());
        assertEquals(createdPlayer.getSurname(), transferResult.getPlayer().getSurname());
        assertEquals(createdPlayer.getBirthdate(), transferResult.getPlayer().getBirthdate());
        assertEquals(1, transferResult.getPlayer().getPlayerTeams().size());
        assertTrue(
                transferResult.getPlayer().getPlayerTeams().stream()
                        .anyMatch(pt -> createdTeam.getId().equals(pt.getTeamId().getId())
                                && LocalDate.now().equals(pt.getMemberFrom())
                                && Objects.isNull(pt.getMemberTo())),
                "Player teams should have updated team"
        );
        assertEquals(BigDecimal.ZERO, transferResult.getTransferFee());
        assertEquals(BigDecimal.ZERO, transferResult.getTransferCommission());
        assertEquals(BigDecimal.ZERO, transferResult.getTotal());
    }

    @Test
    @Transactional(Transactional.TxType.REQUIRED)
    void shouldUpdatePreviousTeamAndAddNewOneOnTransfer() {
        final TeamEntity createdTeam = teamService.create(validTeam);
        final PlayerEntity createdPlayer = playerService.create(validPlayer);
        em.clear();
        associatePlayerWithTeam(createdPlayer, createdTeam);
        final PlayerEntity updatedPlayer = playerService.update(createdPlayer);
        final TeamEntity createdTeam2 = teamService.create(validTeam2);
        em.clear();
        final long testMonthsPassed = ChronoUnit.MONTHS.between(exampleDateFromPast, LocalDate.now());
        final long testYearsPassed = ChronoUnit.YEARS.between(exampleDateFromPast, LocalDate.now());
        final BigDecimal expectedTransferFee = BigDecimal.valueOf(testMonthsPassed * 100_000 / testYearsPassed);
        final BigDecimal expectedTransferCommission = expectedTransferFee.divide(BigDecimal.valueOf(10), RoundingMode.HALF_EVEN);
        final BigDecimal expectedTransferTotal = expectedTransferFee.add(expectedTransferCommission);

        final PlayerTransferResult transferResult = assertDoesNotThrow(() -> playerService.transferTeam(updatedPlayer.getId(), createdTeam2.getId()));

        assertEquals(createdPlayer.getId(), transferResult.getPlayer().getId());
        assertEquals(createdPlayer.getName(), transferResult.getPlayer().getName());
        assertEquals(createdPlayer.getSurname(), transferResult.getPlayer().getSurname());
        assertEquals(createdPlayer.getBirthdate(), transferResult.getPlayer().getBirthdate());
        assertEquals(2, transferResult.getPlayer().getPlayerTeams().size());
        assertTrue(
                transferResult.getPlayer().getPlayerTeams().stream()
                        .anyMatch(pt -> createdTeam.getId().equals(pt.getTeamId().getId())
                                && exampleDateFromPast.equals(pt.getMemberFrom())
                                && LocalDate.now().equals(pt.getMemberTo())),
                "Player teams should have updated team"
        );
        assertTrue(
                transferResult.getPlayer().getPlayerTeams().stream()
                        .anyMatch(pt -> createdTeam2.getId().equals(pt.getTeamId().getId())
                                && LocalDate.now().equals(pt.getMemberFrom())
                                && Objects.isNull(pt.getMemberTo())),
                "Player teams should have new team"
        );
        assertEquals(expectedTransferFee, transferResult.getTransferFee());
        assertEquals(expectedTransferCommission, transferResult.getTransferCommission());
        assertEquals(expectedTransferTotal, transferResult.getTotal());
    }
}