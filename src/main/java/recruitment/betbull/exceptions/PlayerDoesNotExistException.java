package recruitment.betbull.exceptions;

import lombok.AllArgsConstructor;

@AllArgsConstructor
public class PlayerDoesNotExistException extends RuntimeException {
    private final Long playerId;

    @Override
    public String getMessage() {
        return "Player with id " + playerId + " does not exist";
    }
}
