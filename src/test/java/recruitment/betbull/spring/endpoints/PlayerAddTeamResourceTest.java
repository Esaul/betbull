package recruitment.betbull.spring.endpoints;

import com.fasterxml.jackson.annotation.JsonAutoDetect;
import com.fasterxml.jackson.annotation.PropertyAccessor;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.datatype.jsr310.JavaTimeModule;
import lombok.SneakyThrows;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Tag;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.MockMvc;
import recruitment.betbull.constants.TestTags;
import recruitment.betbull.data.base.Player;
import recruitment.betbull.data.base.Team;
import recruitment.betbull.data.dto.PlayerForm;
import recruitment.betbull.data.entity.PlayerEntity;
import recruitment.betbull.data.entity.TeamEntity;
import recruitment.betbull.spring.repositories.PlayerRepository;
import recruitment.betbull.spring.repositories.TeamRepository;

import java.time.LocalDate;
import java.util.Currency;
import java.util.Optional;

import static org.hamcrest.Matchers.*;
import static org.mockito.Mockito.any;
import static org.mockito.Mockito.*;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.put;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@SpringBootTest
@AutoConfigureMockMvc(printOnlyOnFailure = false)
@Tag(TestTags.INTEGRATION)
class PlayerAddTeamResourceTest {
    private static final Long playerId = 1L;
    private static final Long teamId = 1L;
    private static final String existingPlayerName = "existingName";
    private static final String existingPlayerSurname = "existingSurname";
    private static final LocalDate existingPlayerBirthdate = LocalDate.of(2000, 10, 10);
    private static final String existingTeamName = "existingTeam";
    private static final Currency existingTeamCurrency = Currency.getInstance("PLN");
    private final ObjectMapper objectMapper;

    @MockBean
    PlayerRepository playerRepository;

    @MockBean
    TeamRepository teamRepository;

    @Autowired
    MockMvc mockMvc;

    PlayerAddTeamResourceTest() {
        objectMapper = new ObjectMapper();
        objectMapper.setVisibility(PropertyAccessor.FIELD, JsonAutoDetect.Visibility.ANY);
        objectMapper.registerModule(new JavaTimeModule());
    }

    @BeforeEach
    public void setUp() {
        reset(playerRepository, teamRepository);

        PlayerEntity existingPlayer = new PlayerEntity(playerId, new Player(existingPlayerName, existingPlayerSurname, existingPlayerBirthdate));
        TeamEntity existingTeam = new TeamEntity(teamId, new Team(existingTeamName, existingTeamCurrency));

        when(playerRepository.findById(playerId)).thenReturn(Optional.of(existingPlayer));
        when(playerRepository.save(any())).thenAnswer(invocation -> invocation.getArgument(0));
        when(teamRepository.findById(teamId)).thenReturn(Optional.of(existingTeam));
    }

    @SneakyThrows
    @Test
    void shouldReturnValidationErrorsOnNoInput() {
        mockMvc.perform(
                put("/player/addTeam")
                        .contentType(MediaType.APPLICATION_JSON)
        )
        .andExpect(status().isBadRequest())
        .andExpect(jsonPath("$", hasSize(2)))
        .andExpect(jsonPath("$", containsInAnyOrder("player ID is required", "team ID is required")));
    }

    @SneakyThrows
    @Test
    void shouldReturnValidationErrorsOnInvalidPlayerId() {
        final String invalidPlayerId = "invalidPlayerId";
        final PlayerForm fullyInvalidPlayer = new PlayerForm(null, null, null);

        mockMvc.perform(
                put("/player/addTeam")
                        .param("playerId", invalidPlayerId)
                        .param("teamId", teamId.toString())
                        .contentType(MediaType.APPLICATION_JSON)
                        .content(objectMapper.writeValueAsString(fullyInvalidPlayer))
        )
        .andExpect(status().isBadRequest())
        .andExpect(jsonPath("$", is("Cannot parse value to number (value: \"" + invalidPlayerId + "\")")));
    }

    @SneakyThrows
    @Test
    void shouldReturnValidationErrorsOnInvalidTeamId() {
        final String invalidTeamId = "invalidPlayerId";
        final PlayerForm fullyInvalidPlayer = new PlayerForm(null, null, null);

        mockMvc.perform(
                put("/player/addTeam")
                        .param("playerId", playerId.toString())
                        .param("teamId", invalidTeamId)
                        .contentType(MediaType.APPLICATION_JSON)
                        .content(objectMapper.writeValueAsString(fullyInvalidPlayer))
        )
        .andExpect(status().isBadRequest())
        .andExpect(jsonPath("$", is("Cannot parse value to number (value: \"" + invalidTeamId + "\")")));
    }

    @SneakyThrows
    @Test
    void shouldReturnUpdatedPlayerOnValidIds() {
        mockMvc.perform(
                put("/player/addTeam")
                        .param("playerId", playerId.toString())
                        .param("teamId", teamId.toString())
        )
        .andExpect(status().isOk())
        .andExpect(jsonPath("$.id", is(playerId.intValue())))
        .andExpect(jsonPath("$.name", is(existingPlayerName)))
        .andExpect(jsonPath("$.surname", is(existingPlayerSurname)))
        .andExpect(jsonPath("$.birthdate", is(existingPlayerBirthdate.toString())))
        .andExpect(jsonPath("$.playerTeams", hasSize(1)))
        .andExpect(jsonPath("$.playerTeams[0].team.id", is(teamId.intValue())))
        .andExpect(jsonPath("$.playerTeams[0].team.name", is(existingTeamName)))
        .andExpect(jsonPath("$.playerTeams[0].team.currency", is(existingTeamCurrency.getCurrencyCode())))
        .andExpect(jsonPath("$.playerTeams[0].memberFrom", is(LocalDate.now().toString())))
        .andExpect(jsonPath("$.playerTeams[0].memberTo").doesNotExist());
    }
}