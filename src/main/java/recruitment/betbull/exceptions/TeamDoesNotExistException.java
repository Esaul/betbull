package recruitment.betbull.exceptions;

import lombok.AllArgsConstructor;

@AllArgsConstructor
public class TeamDoesNotExistException extends RuntimeException {
    private final Long teamId;

    @Override
    public String getMessage() {
        return "Team with id " + teamId + " does not exist";
    }
}
