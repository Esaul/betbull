package recruitment.betbull.data.serializers;

import com.fasterxml.jackson.core.JsonGenerator;
import com.fasterxml.jackson.databind.SerializerProvider;
import com.fasterxml.jackson.databind.ser.std.StdSerializer;
import recruitment.betbull.data.entity.PlayerTeamEntity;

import java.io.IOException;
import java.util.Set;

public class PlayerTeamTeamsSerializer extends StdSerializer<Set<PlayerTeamEntity>> {
    public PlayerTeamTeamsSerializer() {
        this(null);
    }

    public PlayerTeamTeamsSerializer(Class<Set<PlayerTeamEntity>> t) {
        super(t);
    }

    @Override
    public void serialize(Set<PlayerTeamEntity> value, JsonGenerator gen, SerializerProvider provider) throws IOException {
        gen.writeStartArray();
        for(PlayerTeamEntity playerTeam : value) {
            gen.writeStartObject();
            gen.writeObjectField("team", playerTeam.getTeamId());
            gen.writeObjectField("memberFrom", playerTeam.getMemberFrom());
            if(playerTeam.getMemberTo() != null) {
                gen.writeObjectField("memberTo", playerTeam.getMemberTo());
            }
            gen.writeEndObject();
        }
        gen.writeEndArray();
    }
}
