package recruitment.betbull.spring.services;

import lombok.AllArgsConstructor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import recruitment.betbull.data.base.Team;
import recruitment.betbull.data.entity.TeamEntity;
import recruitment.betbull.exceptions.TeamDoesNotExistException;
import recruitment.betbull.spring.repositories.TeamRepository;

import javax.transaction.Transactional;

@Service
@AllArgsConstructor(onConstructor = @__(@Autowired))
public class TeamService {
    final TeamRepository teamRepository;

    @Transactional(Transactional.TxType.REQUIRED)
    public Page<TeamEntity> findAll(final Pageable page) {
        return teamRepository.findAll(page);
    }

    @Transactional(Transactional.TxType.REQUIRED)
    public TeamEntity findById(final Long id) {
        return teamRepository.findById(id).orElseThrow(() -> new TeamDoesNotExistException(id));
    }

    @Transactional(Transactional.TxType.REQUIRED)
    public TeamEntity create(final Team team) {
        final TeamEntity teamEntity = new TeamEntity(team);
        return create(teamEntity);
    }

    @Transactional(Transactional.TxType.REQUIRED)
    public TeamEntity create(final TeamEntity team) {
        return teamRepository.save(team);
    }

    @Transactional(Transactional.TxType.REQUIRED)
    public TeamEntity update(final Long id, final Team team) {
        final TeamEntity teamEntity = new TeamEntity(id, team);
        return update(teamEntity);
    }

    @Transactional(Transactional.TxType.REQUIRED)
    public TeamEntity update(TeamEntity teamEntity) {
        return teamRepository.save(teamEntity);
    }

    @Transactional(Transactional.TxType.REQUIRED)
    public void delete(final Long id) {
        teamRepository.deleteById(id);
    }
}
