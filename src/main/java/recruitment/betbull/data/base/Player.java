package recruitment.betbull.data.base;

import lombok.*;
import lombok.experimental.SuperBuilder;

import java.time.LocalDate;

@NoArgsConstructor
@AllArgsConstructor
@Getter
@Setter
@SuperBuilder(toBuilder = true)
public class Player {
    private String name;
    private String surname;
    private LocalDate birthdate;

    public Player(final Player other) {
        this.name = other.getName();
        this.surname = other.getSurname();
        this.birthdate = other.getBirthdate();
    }
}
