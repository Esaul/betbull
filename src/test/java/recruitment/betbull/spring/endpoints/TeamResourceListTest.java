package recruitment.betbull.spring.endpoints;

import lombok.SneakyThrows;
import org.hamcrest.Matchers;
import org.junit.jupiter.api.Tag;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.PageRequest;
import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.MockMvc;
import recruitment.betbull.constants.TestTags;
import recruitment.betbull.data.base.Team;
import recruitment.betbull.data.entity.TeamEntity;
import recruitment.betbull.spring.repositories.TeamRepository;

import java.util.Currency;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;
import java.util.stream.IntStream;

import static org.hamcrest.Matchers.*;
import static org.mockito.Mockito.*;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@SpringBootTest
@AutoConfigureMockMvc(printOnlyOnFailure = false)
@Tag(TestTags.INTEGRATION)
class TeamResourceListTest {
    @MockBean
    TeamRepository teamRepository;

    @Autowired
    MockMvc mockMvc;

    private List<TeamEntity> generateTeams() {
        return IntStream.range(0, 3)
            .mapToObj(id -> new TeamEntity((long) id, new Team("exampleName" + id, Currency.getInstance("PLN"))))
            .collect(Collectors.toList());
    }

    @SneakyThrows
    @Test
    void shouldReturnPageOfTeamsOnValidPageNumber() {
        final int pageNumber = 0;
        final List<TeamEntity> existingTeams = generateTeams();
        final PageRequest requestedPage = PageRequest.of(pageNumber, 20);
        final Page<TeamEntity> page = new PageImpl<>(existingTeams, requestedPage, 3);

        doReturn(page).when(teamRepository).findAll(requestedPage);

        mockMvc.perform(
                get("/team/all/" + pageNumber)
                        .contentType(MediaType.APPLICATION_JSON)
        )
        .andExpect(status().isOk())
        .andExpect(jsonPath("$.teams", hasSize(3)))
        .andExpect(jsonPath("$.page", is(pageNumber)))
        .andExpect(jsonPath("$.elementsPerPage", is(20)))
        .andExpect(jsonPath("$.total", is(3)));
    }

    @SneakyThrows
    @Test
    void shouldReturnErrorResponseOnNegativePageNumber() {
        final int pageNumber = -1;

        mockMvc.perform(
                get("/team/all/" + pageNumber)
                        .contentType(MediaType.APPLICATION_JSON)
        )
        .andExpect(status().isBadRequest())
        .andExpect(jsonPath("$", hasSize(1)))
        .andExpect(jsonPath("$", Matchers.contains("page number must be present not negative number")));
    }

    @SneakyThrows
    @Test
    void shouldReturnErrorResponseOnInvalidPageNumber() {
        final String invalidNumber = "invalid";

        mockMvc.perform(
                get("/team/all/" + invalidNumber)
                        .contentType(MediaType.APPLICATION_JSON)
        )
        .andExpect(status().isBadRequest())
        .andExpect(jsonPath("$", is("Cannot parse value to number (value: \"" + invalidNumber + "\")")));
    }

    @SneakyThrows
    @Test
    void shouldReturnErrorResponseOnNonExistingTeamId() {
        final Long nonExistingId = 10L;

        when(teamRepository.findById(nonExistingId)).thenReturn(Optional.empty());

        mockMvc.perform(
                get("/team/" + nonExistingId)
                        .contentType(MediaType.APPLICATION_JSON)
        )
        .andExpect(status().isBadRequest())
        .andExpect(jsonPath("$", is("Team with id " + nonExistingId + " does not exist")));
    }

    @SneakyThrows
    @Test
    void shouldReturnErrorResponseOnNonInvalidTeamId() {
        final String invalidTeamId = "invalid";
        mockMvc.perform(
                get("/team/" + invalidTeamId)
                        .contentType(MediaType.APPLICATION_JSON)
        )
        .andExpect(status().isBadRequest())
        .andExpect(jsonPath("$", is("Cannot parse value to number (value: \"" + invalidTeamId + "\")")));
    }
}