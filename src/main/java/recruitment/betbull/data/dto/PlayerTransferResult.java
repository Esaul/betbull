package recruitment.betbull.data.dto;

import lombok.AllArgsConstructor;
import lombok.Getter;
import recruitment.betbull.data.entity.PlayerEntity;
import recruitment.betbull.data.entity.PlayerTeamEntity;

import java.math.BigDecimal;
import java.math.RoundingMode;
import java.time.LocalDate;
import java.time.temporal.ChronoUnit;
import java.util.Currency;

@AllArgsConstructor
@Getter
public class PlayerTransferResult {
    private final PlayerEntity player;
    private final BigDecimal transferFee;
    private final BigDecimal transferCommission;
    private final BigDecimal total;
    private final Currency currency;

    public PlayerTransferResult(final PlayerEntity updatedPlayer) {
        this.player = updatedPlayer;
        this.currency = null;
        this.transferFee = BigDecimal.ZERO;
        this.transferCommission = BigDecimal.ZERO;
        this.total = BigDecimal.ZERO;
    }

    public PlayerTransferResult(final PlayerEntity updatedPlayer, final PlayerTeamEntity previousTeam) {
        this.player = updatedPlayer;
        this.currency = previousTeam.getTeamId().getCurrency();
        final long monthsOfExperience = ChronoUnit.MONTHS.between(previousTeam.getMemberFrom(), previousTeam.getMemberTo());
        final long age = ChronoUnit.YEARS.between(player.getBirthdate(), LocalDate.now());
        this.transferFee = BigDecimal.valueOf(monthsOfExperience * 100_000 / age);
        this.transferCommission = this.transferFee.divide(BigDecimal.valueOf(10), RoundingMode.HALF_EVEN);
        this.total = this.transferFee.add(this.transferCommission);
    }
}
