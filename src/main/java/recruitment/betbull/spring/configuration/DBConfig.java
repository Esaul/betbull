package recruitment.betbull.spring.configuration;

import org.hibernate.cfg.AvailableSettings;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.autoconfigure.domain.EntityScan;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;

import javax.persistence.EntityManager;

@Configuration
@EnableJpaRepositories("recruitment.betbull.spring.repositories")
@EntityScan("recruitment.betbull.data.entity")
public class DBConfig {
    @Value("${hibernate.dialect:org.hibernate.dialect.H2Dialect}")
    private String dialect;

    @Value("${hibernate.hbm2ddl.auto:update}")
    private String hbm2ddl;

    @Value("${hibernate.show_sql:true}")
    private boolean showSql;

    @Value("${hibernate.format_sql:true}")
    private boolean formatSql;

    @Value("${hibernate.globally_quoted_identifiers:true}")
    private boolean globallyQuotedIdentifiers;

    @Bean
    EntityManager entityManager(final EntityManager em) {
        em.setProperty(AvailableSettings.DIALECT, dialect);
        em.setProperty(AvailableSettings.HBM2DDL_AUTO, hbm2ddl);
        em.setProperty(AvailableSettings.SHOW_SQL, showSql);
        em.setProperty(AvailableSettings.FORMAT_SQL, formatSql);
        em.setProperty(AvailableSettings.GLOBALLY_QUOTED_IDENTIFIERS, globallyQuotedIdentifiers);
        return em;
    }
}
