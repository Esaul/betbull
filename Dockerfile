FROM openjdk:8

EXPOSE 8080

CMD ["java", "-jar", "/betbull.jar", "--spring.config.location=/application.properties"]