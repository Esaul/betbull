jarName=$(ls target/betbull*.jar)
absolutePath=$(realpath "${jarName}")

ipMapping="127.0.0.1"

for ARGUMENT in "$@"; do
  case "$ARGUMENT" in
    expose)
      ipMapping=""
      ;;
  esac
  shift
done

portMapping="${ipMapping}:8080:8080"

docker run -d --rm \
  --name betbull \
  -p "${portMapping}" \
  -v "${absolutePath}":/betbull.jar:ro \
  betbull