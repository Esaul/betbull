package recruitment.betbull.spring.endpoints;

import lombok.SneakyThrows;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Tag;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.MockMvc;
import recruitment.betbull.constants.TestTags;
import recruitment.betbull.data.base.Player;
import recruitment.betbull.data.base.Team;
import recruitment.betbull.data.entity.PlayerEntity;
import recruitment.betbull.data.entity.PlayerTeamEntity;
import recruitment.betbull.data.entity.TeamEntity;
import recruitment.betbull.data.entity.composites.PlayerTeamId;
import recruitment.betbull.spring.repositories.PlayerRepository;
import recruitment.betbull.spring.repositories.TeamRepository;

import java.math.BigDecimal;
import java.math.RoundingMode;
import java.time.LocalDate;
import java.time.temporal.ChronoUnit;
import java.util.Currency;
import java.util.HashSet;
import java.util.Optional;

import static org.hamcrest.Matchers.*;
import static org.mockito.Mockito.any;
import static org.mockito.Mockito.*;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.put;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@SpringBootTest
@AutoConfigureMockMvc(printOnlyOnFailure = false)
@Tag(TestTags.INTEGRATION)
class PlayerTransferResourceTest {
    private static final Long playerId = 1L;
    private static final Long playerWithTeamId = 2L;
    private static final Long teamId = 1L;
    private static final Long previousTeamId = 2L;
    private static final String existingPlayerName = "existingName";
    private static final String existingPlayerSurname = "existingSurname";
    private static final LocalDate dateFromPast = LocalDate.of(2020, 1, 1);
    private static final String existingTeamName = "existingTeam";
    private static final String previousTeamName = "previousTeam";
    private static final Currency existingTeamCurrency = Currency.getInstance("PLN");
    private static final Currency previousTeamCurrency = Currency.getInstance("USD");

    @MockBean
    PlayerRepository playerRepository;

    @MockBean
    TeamRepository teamRepository;

    @Autowired
    MockMvc mockMvc;

    @BeforeEach
    public void setUp() {
        reset(playerRepository, teamRepository);

        final PlayerEntity existingPlayer = new PlayerEntity(playerId, new Player(existingPlayerName, existingPlayerSurname, dateFromPast));
        final TeamEntity existingTeam = new TeamEntity(teamId, new Team(existingTeamName, existingTeamCurrency));
        final TeamEntity previousTeam = new TeamEntity(previousTeamId, new Team(previousTeamName, previousTeamCurrency));
        final PlayerEntity existingPlayerWithTeam = new PlayerEntity(playerWithTeamId, existingPlayer);
        final HashSet<PlayerTeamEntity> playerTeamAssociation = new HashSet<>();
        playerTeamAssociation.add(
                new PlayerTeamEntity(new PlayerTeamId(playerWithTeamId, previousTeamId), existingPlayerWithTeam, previousTeam, dateFromPast, null)
        );
        existingPlayerWithTeam.setPlayerTeams(playerTeamAssociation);

        when(playerRepository.findById(playerId)).thenReturn(Optional.of(existingPlayer));
        when(playerRepository.findById(playerWithTeamId)).thenReturn(Optional.of(existingPlayerWithTeam));
        when(playerRepository.save(any())).thenAnswer(invocation -> invocation.getArgument(0));
        when(teamRepository.findById(teamId)).thenReturn(Optional.of(existingTeam));
        when(teamRepository.findById(previousTeamId)).thenReturn(Optional.of(previousTeam));
    }

    @SneakyThrows
    @Test
    void shouldReturnValidationErrorsOnNoInput() {
        mockMvc.perform(
                put("/player/transfer")
                        .contentType(MediaType.APPLICATION_JSON)
        )
        .andExpect(status().isBadRequest())
        .andExpect(jsonPath("$", hasSize(2)))
        .andExpect(jsonPath("$", containsInAnyOrder("player ID is required", "new team ID is required")));
    }

    @SneakyThrows
    @Test
    void shouldReturnValidationErrorsOnInvalidPlayerId() {
        final String invalidPlayerId = "invalidPlayerId";

        mockMvc.perform(
                put("/player/transfer")
                        .param("playerId", invalidPlayerId)
                        .param("newTeamId", teamId.toString())
                        .contentType(MediaType.APPLICATION_JSON)
        )
        .andExpect(status().isBadRequest())
        .andExpect(jsonPath("$", is("Cannot parse value to number (value: \"" + invalidPlayerId + "\")")));
    }

    @SneakyThrows
    @Test
    void shouldReturnValidationErrorsOnInvalidTeamId() {
        final String invalidTeamId = "invalidPlayerId";

        mockMvc.perform(
                put("/player/transfer")
                        .param("playerId", playerId.toString())
                        .param("newTeamId", invalidTeamId)
                        .contentType(MediaType.APPLICATION_JSON)
        )
        .andExpect(status().isBadRequest())
        .andExpect(jsonPath("$", is("Cannot parse value to number (value: \"" + invalidTeamId + "\")")));
    }

    @SneakyThrows
    @Test
    void shouldReturnUpdatedPlayerOnValidIds() {
        mockMvc.perform(
                put("/player/transfer")
                        .param("playerId", playerId.toString())
                        .param("newTeamId", teamId.toString())
        )
        .andExpect(status().isOk())
        .andExpect(jsonPath("$.player.id", is(playerId.intValue())))
        .andExpect(jsonPath("$.player.name", is(existingPlayerName)))
        .andExpect(jsonPath("$.player.surname", is(existingPlayerSurname)))
        .andExpect(jsonPath("$.player.birthdate", is(dateFromPast.toString())))
        .andExpect(jsonPath("$.player.playerTeams", hasSize(1)))
        .andExpect(jsonPath("$.player.playerTeams[0].team.id", is(teamId.intValue())))
        .andExpect(jsonPath("$.player.playerTeams[0].team.name", is(existingTeamName)))
        .andExpect(jsonPath("$.player.playerTeams[0].team.currency", is(existingTeamCurrency.getCurrencyCode())))
        .andExpect(jsonPath("$.player.playerTeams[0].memberFrom", is(LocalDate.now().toString())))
        .andExpect(jsonPath("$.player.playerTeams[0].memberTo").doesNotExist())
        .andExpect(jsonPath("$.transferFee", is(0)))
        .andExpect(jsonPath("$.transferCommission", is(0)))
        .andExpect(jsonPath("$.total", is(0)))
        .andExpect(jsonPath("$.currency", nullValue()));
    }

    @SneakyThrows
    @Test
    void shouldReturnUpdatedPlayerOnValidIdsForPlayerHavingATeam() {
        final long testMonthsPassed = ChronoUnit.MONTHS.between(dateFromPast, LocalDate.now());
        final long testYearsPassed = ChronoUnit.YEARS.between(dateFromPast, LocalDate.now());
        final BigDecimal expectedTransferFee = BigDecimal.valueOf(testMonthsPassed * 100_000 / testYearsPassed);
        final BigDecimal expectedTransferCommission = expectedTransferFee.divide(BigDecimal.valueOf(10), RoundingMode.HALF_EVEN);
        final BigDecimal expectedTransferTotal = expectedTransferFee.add(expectedTransferCommission);

        mockMvc.perform(
                put("/player/transfer")
                        .param("playerId", playerWithTeamId.toString())
                        .param("newTeamId", teamId.toString())
        )
        .andExpect(status().isOk())
        .andExpect(jsonPath("$.player.id", is(playerWithTeamId.intValue())))
        .andExpect(jsonPath("$.player.name", is(existingPlayerName)))
        .andExpect(jsonPath("$.player.surname", is(existingPlayerSurname)))
        .andExpect(jsonPath("$.player.birthdate", is(dateFromPast.toString())))
        .andExpect(jsonPath("$.player.playerTeams", hasSize(2)))
        .andExpect(jsonPath("$.player.playerTeams[?(@.team.id == " + previousTeamId.intValue() +
                " && @.team.name == \"" + previousTeamName + "\"" +
                " && @.team.currency == \"" + previousTeamCurrency.getCurrencyCode() + "\"" +
                " && @.memberFrom == \"" + dateFromPast + "\"" +
                " && @.memberTo == \"" + LocalDate.now() +"\")]").exists())
        .andExpect(jsonPath("$.player.playerTeams[?(@.team.id == " + teamId.intValue() +
                " && @.team.name == \"" + existingTeamName + "\"" +
                " && @.team.currency == \"" + existingTeamCurrency.getCurrencyCode() + "\"" +
                " && @.memberFrom == \"" + LocalDate.now() + "\")]").exists())
        .andExpect(jsonPath("$.transferFee", is(expectedTransferFee.intValue())))
        .andExpect(jsonPath("$.transferCommission", is(expectedTransferCommission.intValue())))
        .andExpect(jsonPath("$.total", is(expectedTransferTotal.intValue())))
        .andExpect(jsonPath("$.currency", is(previousTeamCurrency.toString())));
    }
}