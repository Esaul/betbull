package recruitment.betbull.data.entity.composites;

import lombok.*;

import javax.persistence.Embeddable;
import java.io.Serializable;

@Embeddable
@NoArgsConstructor
@AllArgsConstructor
@Getter
@Setter
@EqualsAndHashCode
@ToString
public class PlayerTeamId implements Serializable {
    private Long playerId;
    private Long teamId;
}
